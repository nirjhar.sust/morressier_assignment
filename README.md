# Poster Search client for Morressier

This project is designed to be an awesome poster search client, built with ReactJS & Redux.
![](https://i.ibb.co/p1SMc23/Screen-Shot-2019-04-29-at-5-14-21-PM.png[/img])

### [Demo](https://vibrant-curie-245ad6.netlify.com/)
(This is actually hosted using Netlify configured with the Gitlab repository.)
### Clone
```sh
$ git clone git@gitlab.com:nirjhar.sust/morressier_assignment.git
$ cd morressier_assignment
```

### Install dependencies & run
```sh
$ npm install
$ npm run start
```
To access, browse to http://localhost:3000
### Build
```sh
$ npm run build
```
### Key features
  - Search posters using the Search bar at welcomepage/navbar (in other pages).
  - Navigate through the posters using the Previous and Next buttons.
  - Open a poster through clicking on a poster from the results. (As it is cached, reopening that poster again will not make another request to server).
  - Search bar can get the query from url-params or from store.
  - Everything above with a very nice & intuitive user interface.
