export class Collection {

    private _limit: number;
    private _offset: number;
    private _total: number;
    private _items: string[];
    
    get limit(): number {
        return this._limit;
    }

    set limit(value: number) {
        this._limit = value;
    }

    get offset(): number {
        return this._offset;
    }

    set offset(value: number) {
        this._offset = value;
    }

    get total(): number {
        return this._total;
    }

    set total(value: number) {
        this._total = value;
    }

    get items(): string[] {
        return this._items;
    }

    set items(value: string[]) {
        this._items = value;
    }
}
