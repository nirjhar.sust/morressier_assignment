export class MorressierPoster {

    private _id: string;
    private _author_names: string[];
    private _event: string;
    private _keywords: string[];
    private _paper_abstract: string;
    private _public_access_enabled: boolean;
    private _submission_completed: boolean;
    private _title: string;
    private _uploaded_at: string;
    private _thumb_url: string;
    private _thumb_url_medium: string;
    private _thumb_url_large: string;


    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get author_names(): string[] {
        return this._author_names;
    }

    set author_names(value: string[]) {
        this._author_names = value;
    }

    get event(): string {
        return this._event;
    }

    set event(value: string) {
        this._event = value;
    }

    get keywords(): string[] {
        return this._keywords;
    }

    set keywords(value: string[]) {
        this._keywords = value;
    }

    get paper_abstract(): string {
        return this._paper_abstract;
    }

    set paper_abstract(value: string) {
        this._paper_abstract = value;
    }

    get public_access_enabled(): boolean {
        return this._public_access_enabled;
    }

    set public_access_enabled(value: boolean) {
        this._public_access_enabled = value;
    }

    get submission_completed(): boolean {
        return this._submission_completed;
    }

    set submission_completed(value: boolean) {
        this._submission_completed = value;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get uploaded_at(): string {
        return this._uploaded_at;
    }

    set uploaded_at(value: string) {
        this._uploaded_at = value;
    }

    get thumb_url(): string {
        return this._thumb_url;
    }

    set thumb_url(value: string) {
        this._thumb_url = value;
    }

    get thumb_url_medium(): string {
        return this._thumb_url_medium;
    }

    set thumb_url_medium(value: string) {
        this._thumb_url_medium = value;
    }

    get thumb_url_large(): string {
        return this._thumb_url_large;
    }

    set thumb_url_large(value: string) {
        this._thumb_url_large = value;
    }
}
