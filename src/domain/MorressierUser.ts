export class MorressierUser {

    private _id: string;
    private _title: string;
    private _full_name: string;
    private _picture_url: string;
    private _is_activated: boolean;
    private _organization: string;
    private _department: string;
    private _organization_location: string;

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get full_name(): string {
        return this._full_name;
    }

    set full_name(value: string) {
        this._full_name = value;
    }

    get picture_url(): string {
        return this._picture_url;
    }

    set picture_url(value: string) {
        this._picture_url = value;
    }

    get is_activated(): boolean {
        return this._is_activated;
    }

    set is_activated(value: boolean) {
        this._is_activated = value;
    }

    get organization(): string {
        return this._organization;
    }

    set organization(value: string) {
        this._organization = value;
    }

    get department(): string {
        return this._department;
    }

    set department(value: string) {
        this._department = value;
    }

    get organization_location(): string {
        return this._organization_location;
    }

    set organization_location(value: string) {
        this._organization_location = value;
    }
}
