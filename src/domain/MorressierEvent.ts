export class MorressierEvent {

    private _id: string;
    private _language: string;
    private _location: string;
    private _name: string;
    private _short_name: string;
    private _start_date: string;
    private _end_date: string;
    private _venue: string;
    private _website_url: string;


    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get language(): string {
        return this._language;
    }

    set language(value: string) {
        this._language = value;
    }

    get location(): string {
        return this._location;
    }

    set location(value: string) {
        this._location = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get short_name(): string {
        return this._short_name;
    }

    set short_name(value: string) {
        this._short_name = value;
    }

    get start_date(): string {
        return this._start_date;
    }

    set start_date(value: string) {
        this._start_date = value;
    }

    get end_date(): string {
        return this._end_date;
    }

    set end_date(value: string) {
        this._end_date = value;
    }

    get venue(): string {
        return this._venue;
    }

    set venue(value: string) {
        this._venue = value;
    }

    get website_url(): string {
        return this._website_url;
    }

    set website_url(value: string) {
        this._website_url = value;
    }
}
