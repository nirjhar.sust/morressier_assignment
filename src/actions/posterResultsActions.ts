import axios from 'axios';
import store from '../store/store';

export function fetchPosters(query: any, page: number) {
    // @ts-ignore
    store.dispatch((dispatch) => {

        dispatch({type: 'FETCH_QUERY_RESULTS'});
        axios.get('/events_manager/v3/posters/search', {
            params: {
                query,
                offset: ((page - 1) || 0)  * 20,
                limit: 20,
            }
        })
        .then((response) => {
            dispatch({type: 'RECEIVED_QUERY_RESULTS', payload: response.data});
        })
        .catch((error) => {
            dispatch({type: 'ERROR_QUERY_RESULTS'});
        });
    });
}
