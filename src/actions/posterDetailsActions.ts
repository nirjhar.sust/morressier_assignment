import axios from 'axios';
import store from '../store/store';

export function fetchPosterDetails(posterId: string) {
    // @ts-ignore
    store.dispatch((dispatch) => {

        dispatch({type: 'FETCH_POSTER_DETAILS', id: posterId});
        if (store.getState().posterDetailsById.hasOwnProperty(posterId)) {
            const payload = store.getState().posterDetailsById[posterId];
            console.log('consoling from memory');
            dispatch({type: 'RECEIVED_POSTER_DETAILS',payload, id: posterId});
        }
        else {
            console.log('consoling from server');

            axios.get('/events_manager/v2/posters/' + posterId)
                .then((response) => {
                    dispatch({type: 'RECEIVED_POSTER_DETAILS', payload: response.data, id: posterId});
                })
                .catch((error) => {
                    dispatch({type: 'ERROR_POSTER_DETAILS'});
                });
        }
    });
}
