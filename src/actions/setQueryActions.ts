import store from '../store/store';

export function setQuery(query: string) {
    store.dispatch({type: 'SET_POSTER_QUERY', payload: query});
}

export function doNewSearch(event: any, props: any, query: string) {
    props.history.push({
        pathname: '/search',
        search: "?" + new URLSearchParams({query, page: '1'}).toString()
    });
}
