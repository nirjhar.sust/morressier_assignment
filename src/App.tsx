import 'bootstrap/dist/css/bootstrap.css';
import * as React from 'react';
import {Provider} from 'react-redux'
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import './App.css';
import PosterDetails from "./containers/PosterDetailsPage/posterDetailsPage";
import PosterResults from "./containers/PosterResultsPage/posterResultsPage";
import WelcomePage from "./containers/WelcomePage/welcomePage";
import store from './store/store';


class App extends React.Component {

    constructor(props: any) {
        super(props);
    }

    public render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <div className="rickety-react-app">
                        <Switch>
                            <Route exact={true} path="/" component={WelcomePage}/>
                            <Route path="/search" component={PosterResults}/>
                            <Route path="/poster/:id" component={PosterDetails}/>
                            <Route render={() => <Redirect to="/" />} />
                        </Switch>
                    </div>
                </BrowserRouter>
            </Provider>
        );
    }
}

export default App;
