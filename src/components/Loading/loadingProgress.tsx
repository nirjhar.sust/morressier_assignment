import * as React from "react";
import './loadingProgress.css';

class LoadingProgress extends React.Component {

    constructor(props: any) {
        super(props);
    }

    public render() {
        return (
            <div className="loading-sign">
                <div className="loading-spinner">
                    <i className="fas fa-spinner fa-w-16 fa-spin fa-lg"/>
                </div>
            </div>
        );
    }
}

export default LoadingProgress;

