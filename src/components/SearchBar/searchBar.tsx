import * as React from "react";
import {connect} from "react-redux";
import { withRouter } from "react-router-dom";
import {setQuery} from "../../actions/setQueryActions";
import './searchBar.css';

interface MyState {
    query: string
}

interface MyProps {
    query: string
    history?: any
    location?: any
}

class SearchBar extends React.Component<MyProps, MyState> {

    constructor(props: any) {
        super(props);
        this.state = {query: ''};
        this.onQueryTextChange = this.onQueryTextChange.bind(this);
        this.onSearchSubmit = this.onSearchSubmit.bind(this);
    }

    public componentWillMount() {
        this.setState({query: this.setQueryFromParams(this.props)});
    }

    public componentDidUpdate(prevProps: any) {
        if (this.props.location !== prevProps.location) {
            this.onRouteChanged(prevProps);
        }
    }

    public onRouteChanged(prevProps: any) {
        this.setQueryFromParams(this.props);
    }

    public setQueryFromParams(props: any) {
        const searchQuery = props.location.search;
        const queryParams = new URLSearchParams(searchQuery);
        const nextPropsQuery = queryParams.get('query') || this.props.query;
        this.setState({query: nextPropsQuery});
        setQuery(nextPropsQuery);
        return nextPropsQuery;
    }

    public onQueryTextChange(event: any) {
        this.setState({query: event.target.value});
        setQuery(event.target.value);
    }

    public onSearchSubmit(event: any) {
        event.preventDefault();
        this.props.history.push({
            pathname: '/search',
            search: "?" + new URLSearchParams({query: this.state.query, page: '1'}).toString()
        });
    }

    public render() {
        return (
            <div className="search-bar">
                <form className="navbar-form" onSubmit={this.onSearchSubmit}>
                    <div className="input-group">
                        <input value={this.state.query} onChange={this.onQueryTextChange} type="text" className="form-control query-input" placeholder="Search for..."/>
                        <span className="input-group-btn">
                        <button className="btn btn-default query-go" type="submit">
                            <i className="fas fa-search" />
                        </button>
                    </span>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state: any, props: any) => {
    return {
        query : state.queryText
    };
};
export default connect(mapStateToProps)(withRouter(SearchBar));

