interface Action {
    type: string;
    payload: any;
    id: string
}

const posterDetailsReducer = (state = {}, action: Action) => {
    switch (action.type) {
        case 'FETCH_POSTER_DETAILS' : {
            return {...state};
        }
        case 'RECEIVED_POSTER_DETAILS' : {
            state[action.id] = action.payload;
            return {...state};
        }
    }
    return state;
};

export default posterDetailsReducer;
