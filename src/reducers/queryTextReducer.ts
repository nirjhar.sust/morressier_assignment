interface Action {
    type: string;
    payload: any;
    id: string
}

const queryTextReducer = (state = '', action: Action) => {
    switch (action.type) {
        case 'SET_POSTER_QUERY' : {
            state = action.payload;
            return state;
        }
    }
    return state;
};

export default queryTextReducer;
