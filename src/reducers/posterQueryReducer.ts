interface Action {
    type: string;
    payload: any;
}

const posterQueryReducer = (state = {}, action: Action) => {
    switch (action.type) {
        case 'FETCH_QUERY_RESULTS' : {
            return {}
        }
        case 'RECEIVED_QUERY_RESULTS' : {
            return {...action.payload};
        }
    }
    return state;
};

export default posterQueryReducer;
