import * as React from "react";
import {Link} from "react-router-dom";
import icon from '../../assets/icons/icon.png';
import logo from '../../assets/icons/morressier.png';
import SearchBar from "../../components/SearchBar/searchBar";
import './navbar.css';

class NavBar extends React.Component {

    constructor(props: any) {
        super(props);
    }

    public render() {
        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <Link to="/" className="navbar-brand">
                            <div>
                                <img alt="Brand" src={logo}/>
                                <span><img alt="Brand" src={icon}/></span>
                            </div>
                        </Link>
                    </div>
                    <div className="navbar-right">
                        <SearchBar/>
                    </div>
                </div>
            </nav>

        );
    }
}

export default NavBar;
