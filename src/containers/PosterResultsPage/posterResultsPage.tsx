import * as React from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {fetchPosters} from "../../actions/posterResultsActions";
import LoadingProgress from "../../components/Loading/loadingProgress";
import {Collection} from "../../domain/Collection";
import {MorressierEvent} from "../../domain/MorressierEvent";
import {MorressierPoster} from "../../domain/MorressierPoster";
import {MorressierUser} from "../../domain/MorressierUser";
import NavBar from "../Navbar/navbar";
import './posterResultsPage.css';

interface MyState {
    query: string
}

interface MyProps {
    history: any
    location: any
    router: any
    collection: Collection
    posters: MorressierPoster[]
    events: MorressierEvent[]
    users: MorressierUser[]
}

class PosterResultsPage extends React.Component<MyProps, MyState> {

    public queryText: any;
    public currentPage: any;

    constructor(props: any) {
        super(props);
        this.gotoNextPage = this.gotoNextPage.bind(this);
        this.gotoPreviousPage = this.gotoPreviousPage.bind(this);
    }

    public componentWillMount() {
        this.processQueryString();
        this.fetchData(this.queryText, this.currentPage);
    }

    public componentWillReceiveProps(nextProps: any) {
        const searchQuery = nextProps.location.search;
        const queryParams = new URLSearchParams(searchQuery);
        const nextPropsQuery = queryParams.get('query') || '';
        const nextPropsPage = parseInt((queryParams.get('page') || 1).toString());
        if (this.queryText !== nextPropsQuery || this.currentPage !== nextPropsPage) {
            this.queryText = nextPropsQuery;
            this.currentPage = nextPropsPage;
            this.fetchData(nextPropsQuery, nextPropsPage);
        }
    }

    public processQueryString() {
        const searchQuery = this.props.location.search;
        const queryParams = new URLSearchParams(searchQuery);
        this.queryText = queryParams.get('query') || '';
        this.currentPage = parseInt((queryParams.get('page') || 1).toString());
    }

    public fetchData(query: any, page: any) {
        if (query.length) {
            fetchPosters(query, page);
        }
        else {
            this.redirectToHomePage();
        }
    }

    public gotoPreviousPage() {
        this.currentPage = this.currentPage > 1 ? this.currentPage - 1 : 1;
        this.searchNew();
    }

    public gotoNextPage() {
        this.currentPage = this.currentPage + 1;
        this.searchNew();
    }

    public searchNew() {
        this.props.history.push({
            pathname: '/search',
            search: "?" + new URLSearchParams({query: this.queryText, page: this.currentPage.toString()}).toString()
        });
        fetchPosters(this.queryText, this.currentPage);
    }

    public redirectToHomePage() {
        console.log('going to home page');
        this.props.history.push({
            pathname: '/'
        });
    }

    public isPreviousButtonDisabled() {
        return this.currentPage === 1;
    }

    public isNextButtonDisabled() {
        const itemsOffset = this.currentPage * 20;
        return this.props.collection && itemsOffset >= this.props.collection.total;
    }

    public render() {

        const hasPosters = this.props.posters && this.props.posters.length;
        let posterLinks : any = [];
        if (hasPosters) {
            posterLinks = this.props.posters.map((poster: MorressierPoster, index: number) => {
                const posterUrl = '/poster/' + poster.id;
                const authors = poster.author_names ? poster.author_names.join(', ') : '';
                const keywords = poster.keywords ? poster.keywords.join(', ') : '';
                return (
                    <Link className="poster-result" to={posterUrl} key={index}>
                        <div className="poster-info">
                            <div className="poster-title" title={poster.title}>{poster.title}</div>
                        </div>
                        <div className="poster-image">
                            <img src={poster.thumb_url}/>
                        </div>
                        <div className="poster-info">
                            <div className="poster-author" title={authors}>{authors}</div>
                            <div className="poster-keyword" title={keywords}>{keywords}</div>
                        </div>
                    </Link>
                )
            });
        }

        const loadingSign = <LoadingProgress/>;
        let activeBody =
            <div>
                <div className="container pagination-buttons">
                    <button disabled={this.isPreviousButtonDisabled()} className="btn btn-default pull-left" onClick={this.gotoPreviousPage}>Previous</button>
                    <button disabled={this.isNextButtonDisabled()} className="btn btn-default pull-right" onClick={this.gotoNextPage}>Next</button>
                </div>
                <div className="container">
                    <div className="poster-results-grid">
                        {posterLinks}
                    </div>
                </div>
            </div>;

        const noDataFound =
            <div className="no-data-found">
                <i className="far fa-sad-tear"/>
                <div>Sorry, no data found</div>
            </div>;

        if (this.props.collection && this.props.collection.total === 0) {
            activeBody = noDataFound;
        }

        return (
            <div className="poster-results-page">
                <NavBar/>
                {!this.props.collection ? loadingSign : activeBody}
            </div>
        );
    }
}
const mapStateToProps = (state: any) => {
    return {
        collection : state.postersQueryResults.collection as Collection,
        posters : state.postersQueryResults.posters as MorressierPoster[],
        events : state.postersQueryResults.events as MorressierEvent[],
        users : state.postersQueryResults.users as MorressierUser[]
    };
};
export default connect(mapStateToProps)(PosterResultsPage);

