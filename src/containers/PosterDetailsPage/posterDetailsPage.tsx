import * as React from "react";
import {connect} from "react-redux";
import {fetchPosterDetails} from "../../actions/posterDetailsActions";
import LoadingProgress from "../../components/Loading/loadingProgress";
import NavBar from "../Navbar/navbar";
import './posterDetailsPage.css';

interface MyProps {
    history: any
    location: any
    router: any
    posters: any
    match: any,
    posterDetails: any
}

class PosterDetailsPage extends React.Component<MyProps> {

    public posterId : string;

    constructor(props: any) {
        super(props);
        this.posterId = this.props.match.params.id;
        console.log(this.props.posterDetails);
    }

    public componentWillMount() {
        this.fetchPosterDetails();
    }

    public fetchPosterDetails() {
        if (this.posterId) {
            fetchPosterDetails(this.posterId);
        }
        else {
            this.redirectToHomePage();
        }
    }

    public redirectToHomePage() {
        console.log('going to home page');
        this.props.history.push({
            pathname: '/'
        });
    }

    public render() {
        if (!this.props.posterDetails) {
            return (
                <div className="poster-details-page">
                    <NavBar/>
                    <LoadingProgress/>
                </div>
            );
        }

        return (
            <div className="poster-details-page">
                <NavBar/>
                <div className="container poster-details-container">
                    <div className="row-fluid">
                        <div className="col-md-4">
                            <div className="row-fluid">
                                <div className="poster-thumbnail">
                                    <img className="poster-thumbnail" src={this.props.posterDetails.poster.thumb_url_medium} />
                                </div>
                            </div>
                            <div className="row-fluid">
                                <div>
                                    <b>Uploaded</b>
                                    <p>{this.props.posterDetails.poster.uploaded_at}</p>
                                </div>
                                <div>
                                    <b>Event name</b>
                                    <p>{this.props.posterDetails.event.name}</p>
                                    <p>{this.props.posterDetails.event.location}</p>
                                    <p>{this.props.posterDetails.event.website_url}</p>
                                </div>
                                <div>
                                    <b>Keywords</b>
                                    <p>{this.props.posterDetails.poster.keywords.join(', ')}</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-8">
                            <div className="row-fluid">
                                <div>
                                    <p className="poster-title">
                                        {this.props.posterDetails.poster.title}
                                    </p>
                                </div>
                            </div>
                            <div className="row-fluid">
                                <div>
                                    <b>Abstract</b>
                                    <p>
                                        {this.props.posterDetails.poster.paper_abstract}
                                    </p>
                                </div>
                            </div>
                            <div className="row-fluid">
                                <div>
                                    <b>Users</b>
                                    <p>
                                        {this.props.posterDetails.users.map((user: any) => user.full_name).join(', ')}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>;
            </div>
        );
    }
}

const mapStateToProps = (state: any, props: any) => {
    const posterId = props.match.params.id;
    return {
        posterDetails: state.posterDetailsById[posterId]
    };
};
export default connect(mapStateToProps)(PosterDetailsPage);
