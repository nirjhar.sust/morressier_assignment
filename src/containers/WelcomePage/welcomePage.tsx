import * as React from "react";
import {Link} from "react-router-dom";
import hero_illustration from '../../assets/icons/hero_illustration.svg';
import icon from '../../assets/icons/icon.png';
import logo from '../../assets/icons/morressier.png';
import SearchBar from "../../components/SearchBar/searchBar";
import './welcomePage.css';

interface MyProps {
    history: any
}

class WelcomePage extends React.Component<MyProps> {

    constructor(props: any) {
        super(props);
    }

    public render() {
        return (
            <div className="welcome-page">
                <div className="container">
                    <div className="row-fluid">
                        <Link to="/" className="logo-container">
                            <img className="logo" src={logo}/>
                            <img className="icon" src={icon}/>
                        </Link>
                    </div>
                    <div className="row-fluid">
                        <div className="col-lg-6 col-md-6 col-sm-6 page-left">
                            <div className="search-section">
                                <div className="search-section-text">
                                    The hassle-free abstract, poster, and presentation management system.
                                </div>
                                <SearchBar/>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-6 page-right">
                            <img className="hero-illustration" src={hero_illustration}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default WelcomePage;
