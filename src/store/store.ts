import {applyMiddleware, combineReducers, createStore} from 'redux';
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from 'redux-thunk';
import posterDetailsReducer from "../reducers/posterDetailsReducer";
import posterQueryReducer from "../reducers/posterQueryReducer";
import queryTextReducer from "../reducers/queryTextReducer";

const middleWares = applyMiddleware(thunk);
const combinedReducer = combineReducers({
    queryText : queryTextReducer,
    postersQueryResults : posterQueryReducer,
    posterDetailsById : posterDetailsReducer
});

export default createStore(combinedReducer,{},composeWithDevTools(middleWares));
